package com.westeroscraft.whitelist;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.plugin.java.JavaPlugin;

public class WhiteListPlugin extends JavaPlugin implements Listener {
    boolean whitelisted;
    @Override
    public void onEnable() {
        // Get configuration
        FileConfiguration cfg = this.getConfig();
        cfg.options().copyDefaults(true);
        this.saveConfig();

        whitelisted = cfg.getBoolean("whitelist-enabled", false);
        if (whitelisted) {
            this.getLogger().info("WhiteList mode enabled - NOBUILDs blocked");
            getServer().getPluginManager().registerEvents(this, this);
        }
        else {
            this.getLogger().info("WhiteList mode disabled");
        }
    }
    
    @EventHandler(priority=EventPriority.HIGH, ignoreCancelled=true)
    public void playerLogin(PlayerLoginEvent event) {
        Player p = event.getPlayer();
        if ((p != null) && (p.hasPermission("whitelist.allowed"))) {
            return;
        }
        event.disallow(Result.KICK_WHITELIST, "Server does not allow NOBUILD players");
    }
}
